﻿#if UNITY_EDITOR
using Hive;
using Hive.Editor;
using Hive.Editor.iOS;
using Hive.Application;
using Hive.Application.Editor;
using System;
using System.IO;
using UnityEngine;
using Hive.InAppPurchases;
using Hive.Editor.Android;

[Serializable]
public sealed class InAppPurchasesExtensionEditor : AppProfileExtensionEditor
{
    private const string PLUGINS_PATH = HiveEditorConst.Paths.AssetsRoot + "/InAppPurchase/Plugins";
    private const string XCODE_PATH = HiveEditorConst.ProjectShortName + "/InAppPurchase";

    public override string Name { get { return "In-App Purchases"; } }
    public override string PreprocessorDirectives { get { return "HIVE_INAPP_PURCHASES"; } }
    public override string PreprocessorDirectivesWhenEnabled { get { return "HIVE_INAPP_PURCHASES_ENABLED"; } }

    protected override void OnPreBuild(Hive.Application.Editor.AppProfileEditor profile, PreBuildArgs args)
    {
        // -- Google Play -----------------------------------------------------------------
        if (profile is GooglePlayAppProfileEditor)
        {
            // Enable native plugins
            args.GradleScript.AddDependency(new AndroidSupportDependency("support-annotations"));
            UnityPluginsHelper.EnablePluginAsset(PLUGINS_PATH + "/GooglePlay/hive-iap-googleplay.aar", profile.BuildTarget);
        }
    }

    protected override void OnPostBuild(Hive.Application.Editor.AppProfileEditor profile, PostBuildArgs args)
    {
        // -- iOS -------------------------------------------------------------------------
#if UNITY_IOS
        if (profile is IosAppProfileEditor)
        {
            ExtractZip(PLUGINS_PATH + "/iOS/HiveInAppPurchases.zip", Path.Combine(args.BuildPath, XCODE_PATH));

            args.PbxProject.AddLib(XCODE_PATH + "/libHiveInAppPurchases.a");
            args.PbxProject.AddFramework(XCODE_PATH + "/OpenSSL.framework");
            args.PbxProject.AddSystemFramework(Frameworks.StoreKit);
        }
#endif
    }

    private static void ExtractZip(string zipFilePath, string destFolder)
    {
        using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(zipFilePath))
        {
            foreach (Ionic.Zip.ZipEntry z in zip)
                z.Extract(destFolder, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
        }
    }
}

public partial class AppProfileEditor
{
    [SerializeField]
    [AppProfileFilter(ProfileType = typeof(IosAppProfile))]
    [AppProfileFilter(ProfileType = typeof(GooglePlayAppProfile))]
    private InAppPurchasesExtensionEditor _inAppPurchases = new InAppPurchasesExtensionEditor();
    public InAppPurchasesExtensionEditor InAppPurchases { get { return _inAppPurchases; } }
}

#endif
