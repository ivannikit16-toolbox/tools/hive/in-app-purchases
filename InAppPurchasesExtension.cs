﻿using Hive;
using Hive.Application;
using Hive.InAppPurchases;
using System;
using UnityEngine;

[Serializable]
public sealed class InAppPurchasesExtension : AppProfileExtension
{
    public override bool CanDisable { get { return true; } }

    /// <summary>
    /// Gets or sets a value indicating whether the Local Receipt Verifier should be enabled on startup or not.
    /// </summary>
    [ToggleLeft]
    [AppProfileFilter(ProfileType = typeof(IosAppProfile))]
    public bool EnableLocalReceiptVerifier = true;

    /// <summary>
    /// Gets or sets a value indicating whether the iTunes Receipt Verifier should be enabled on startup or not.
    /// </summary>
    [ToggleLeft("Enable iTunes Receipt Verifier")]
    [AppProfileFilter(ProfileType = typeof(IosAppProfile))]
    public bool EnableItunesReceiptVerifier = false;

    [ToggleLeft("Request products data on startup")]
    public bool RequestProductsDataOnStartup = false;

    //[AppProfileFilter(EditorOnly = true)]
    public bool SimulateInEditor = true;

    //[AppProfileFilter(EditorOnly = true)]
    public InAppResultCodes SimulatedOperationsResult = InAppResultCodes.Ok;


    [NonSerialized]
    private InAppProductsCollection _products = new InAppProductsCollection();
    public InAppProductsCollection Products { get { return _products; } }
}

public partial class AppProfile
{
    [SerializeField]
    [AppProfileFilter(ProfileType = typeof(IosAppProfile))]
    [AppProfileFilter(ProfileType = typeof(GooglePlayAppProfile))]
    private InAppPurchasesExtension _inAppPurchases = new InAppPurchasesExtension();
    public InAppPurchasesExtension InAppPurchases { get { return _inAppPurchases; } }
}
