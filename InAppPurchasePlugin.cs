﻿using System;
using System.Collections.Generic;

namespace Hive.InAppPurchases
{
    /// <summary>
    /// The class represents In-App Purchase plugin
    /// </summary>
    internal class InAppPurchasePlugin : IHivePlugin
    {
        private readonly InAppPurchasesExtension _settings;

        public InAppPurchasePlugin(InAppPurchasesExtension settings)
        {
            _settings = settings;
        }

        public string Name
        {
            get { return "InAppPurchase"; }
        }

        public void Configure()
        {
        }

        public void Run(IHostRunningContext context)
        {
            InAppPurchases.Run(_settings);

            // added timeout to process iap-callbacks
            HiveFoundation.Instance.Scheduler.AddActionDelayedTask(RunningComplete, .01f, context);
        }

        private void RunningComplete(IHostRunningContext context)
        {
            context.RunNext();
        }
    }

    /// <summary>
    /// The class extends Hive Host Builder to integrate In-App Purchase plugin
    /// </summary>
    public static class InAppPurchasePluginExtension
    {
        /// <summary>
        /// Add In-App Purchase plugin with settings that defined in current AppProfile and with specified collection of products
        /// </summary>
        /// <param name="builder">Instance of Hive Host Builder</param>
        /// <param name="products">In-App Products collection</param>
        public static IHiveHostBuilder AddInAppPurchase(this IHiveHostBuilder builder, IEnumerable<InAppProduct> products)
        {
            var settings = AppProfile.Instance.InAppPurchases;

            if (products != null)
                foreach (var p in products)
                    settings.Products.Add(p);

            return AddInAppPurchase(builder, settings);
        }

        /// <summary>
        /// Add In-App Purchase plugin with settings that defined in current AppProfile and with specified collection of products
        /// </summary>
        /// <param name="builder">Instance of Hive Host Builder</param>
        /// <param name="products">In-App Products collection</param>
        public static IHiveHostBuilder AddInAppPurchase(this IHiveHostBuilder builder, params InAppProduct[] products)
        {
            return AddInAppPurchase(builder, (IEnumerable<InAppProduct>)products);
        }

        /// <summary>
        /// Add In-App Purchase plugin with settings that defined in current AppProfile and modified with setup delegate
        /// </summary>
        /// <param name="builder">Instance of Hive Host Builder</param>
        /// <param name="setup">Delegate to setup plugin settins</param>
        public static IHiveHostBuilder AddInAppPurchase(this IHiveHostBuilder builder, Action<InAppPurchasesExtension /*settings*/> setup)
        {
            var settings = AppProfile.Instance.InAppPurchases;

            if (setup != null)
                setup(settings);

            return AddInAppPurchase(builder, settings);
        }

        /// <summary>
        /// Add In-App Purchase plugin with custom settings.
        /// </summary>
        /// <param name="builder">Instance of Hive Host Builder</param>
        /// <param name="settings">Custom settings</param>
        public static IHiveHostBuilder AddInAppPurchase(this IHiveHostBuilder builder, InAppPurchasesExtension settings)
        {
            // Also you can register aop-joinpoints right here,
            // but we do that in class derived from IHivePlugin by agreement.

            return builder.AddPlugin(new InAppPurchasePlugin(settings));
        }
    }
}
