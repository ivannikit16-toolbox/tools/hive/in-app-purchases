﻿using System;
using UnityEngine;

namespace Hive.InAppPurchases
{
    public enum InAppProductType
    {
        Undefined = 0,
        Consumable,
        NonConsumable,
        AutoRenewableSubscription,
    }

    public enum InAppProductState
    {
        NotFetched = 0,
        OutOfDate,
        Updated
    }

    public class InAppProduct
    {
        public readonly string Identifier;
        public readonly InAppProductType ProductType;
        public string FormattedPrice = null;
        public string CountryCode = null;
        public string CurrencyCode = null;
        public string CurrencySymbol = null;
        public float Price = 0f;
        public InAppProductState State = InAppProductState.NotFetched;

        public InAppProduct(string identifier, InAppProductType productType)
        {
            Identifier = identifier;
            ProductType = productType;
        }

        public DateTime? SubscriptionPurchasingDate { get; internal set; }
        public DateTime? SubscriptionExpirationDate { get; internal set; }
        public DateTime? SubscriptionCancellationDate { get; internal set; }
        public bool SubscriptionActive { get; internal set; }
        public string SubscriptionTransactionId = null;
        public string SubscriptionOriginalTransactionId = null;

        public void Reset()
        {
            FormattedPrice = null;
            CountryCode = null;
            CurrencyCode = null;
            CurrencySymbol = null;
            Price = 0f;
            State = InAppProductState.NotFetched;

            //SubscriptionPurchasingDate = null;
            //SubscriptionExpirationDate = null;
            //SubscriptionCancellationDate = null;
            //SubscriptionActive = false;
        }

        public override string ToString()
        {
            return ProductType == InAppProductType.AutoRenewableSubscription
                ? string.Format("InAppProduct: Id = {0} ({1}), Active = {6}, FormattedPrice = {2}, Price = {3} CurrencyCode = {4} CurrencySymbol = {5}", Identifier, ProductType.ToString(), FormattedPrice, Price, CurrencyCode, CurrencySymbol, SubscriptionActive)
                : string.Format("InAppProduct: Id = {0} ({1}), FormattedPrice = {2}, Price = {3} CurrencyCode = {4} CurrencySymbol = {5}", Identifier, ProductType.ToString(), FormattedPrice, Price, CurrencyCode, CurrencySymbol);
        }
    }
}
