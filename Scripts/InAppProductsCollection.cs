﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Hive.InAppPurchases
{
    /// <summary>
    /// A collection of unique in-app products
    /// </summary>
    public class InAppProductsCollection : IEnumerable<InAppProduct>
    {
        private List<InAppProduct> _products = new List<InAppProduct>();

        /// <summary>
        /// Gets a value indicating whether the collection is read-only
        /// </summary>
        public bool ReadOnly { get; internal set; }

        /// <summary>
        /// Gets the number of products contained in this collection
        /// </summary>
        public int Count
        {
            get { return _products.Count; }
        }

        /// <summary>
        /// Determines whether the collection contains a product with identifier that equals to identifier of specified product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public bool Contains(InAppProduct product)
        {
            if (product == null)
                return false;

            return Contains(product.Identifier);
        }

        /// <summary>
        /// Determines whether a product with specified identifier exists in the collection
        /// </summary>
        /// <param name="productIdentifier"></param>
        /// <returns></returns>
        public bool Contains(string productIdentifier)
        {
            for (int i = 0; i < _products.Count; i++)
                if (_products[i].Identifier == productIdentifier)
                    return true;
            return false;
        }

        /// <summary>
        /// Searches for a product with identifier that equals to identifier of specified product and returns its zero-based index in the collecton
        /// </summary>
        /// <param name="product"></param>
        /// <returns>The zero-based index of the product in the collecton, if found; otherwise, –1</returns>
        public int IndexOf(InAppProduct product)
        {
            if (product == null)
                return -1;

            return IndexOf(product.Identifier);
        }

        /// <summary>
        /// Searches for a product with the specified identifier and returns its zero-based index in the collecton
        /// </summary>
        /// <param name="productIdentifier"></param>
        /// <returns>The zero-based index of the product in the collecton, if found; otherwise, –1</returns>
        public int IndexOf(string productIdentifier)
        {
            for (int i = 0; i < _products.Count; i++)
                if (_products[i].Identifier == productIdentifier)
                    return i;
            return -1;
        }

        /// <summary>
        /// Searches for a product with the specified identifier and returns its instance
        /// </summary>
        /// <param name="productIdentifier">Product identifier you're looking for</param>
        /// <returns>The product instance, if found; otherwise, null</returns>
        public InAppProduct Find(string productIdentifier)
        {
            int idx = IndexOf(productIdentifier);
            return idx >= 0 ? _products[idx] : null;
        }

        /// <summary>
        /// Searches for a products with the specified identifiers and returns them instances
        /// </summary>
        /// <param name="productIdentifiers">Product identifiers you're looking for</param>
        /// <returns>Array of the products, if found; otherwise, empty array</returns>
        public InAppProduct[] Find(string[] productIdentifiers)
        {
            List<InAppProduct> rs = new List<InAppProduct>();

            if (productIdentifiers != null)
                for (int i = 0; i < productIdentifiers.Length; i++)
                {
                    int idx = IndexOf(productIdentifiers[i]);
                    if (idx >= 0)
                        rs.Add(_products[idx]);
                }

            return rs.ToArray();
        }

        /// <summary>
        /// Adds unique product to the collection.
        /// </summary>
        /// <param name="product"></param>
        public void Add(InAppProduct product)
        {
            if (ReadOnly)
                throw new NotSupportedException("Failed to add product. Collection is read-only.");

            if (product == null)
                throw new NullReferenceException("Failed to add product. Argument is null.");

            if (Contains(product.Identifier))
                throw new InvalidOperationException("Failed to add product '" + product.Identifier + "'. It's already exists in the collection.");

            _products.Add(product);
        }

        /// <summary>
        /// Removes product from the collection
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public bool Remove(InAppProduct product)
        {
            if (ReadOnly)
                throw new NotSupportedException("Failed to add product. Collection is read-only.");

            if (product == null)
                throw new NullReferenceException("Failed to remove product. Argument is null.");

            int idx = IndexOf(product);
            if (idx < 0)
                return false;

            _products.RemoveAt(idx);
            return true;
        }

        /// <summary>
        /// Removes product with specified identifier from the collection
        /// </summary>
        /// <param name="productIdentifier"></param>
        /// <returns></returns>
        public bool Remove(string productIdentifier)
        {
            if (ReadOnly)
                throw new NotSupportedException("Failed to add product. Collection is read-only.");

            int idx = IndexOf(productIdentifier);
            if (idx < 0)
                return false;

            _products.RemoveAt(idx);
            return true;
        }

        /// <summary>
        /// Removes product at the specified index of the collection
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            if (ReadOnly)
                throw new NotSupportedException("Failed to add product. Collection is read-only.");

            _products.RemoveAt(index);
        }

        /// <summary>
        /// Removes all products from the collection
        /// </summary>
        public void Clear()
        {
            if (ReadOnly)
                throw new NotSupportedException("Failed to add product. Collection is read-only.");

            _products.Clear();
        }

        /// <summary>
        /// Gets a product by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public InAppProduct this[int index]
        {
            get { return _products[index]; }
        }

        /// <summary>
        /// Resets data of specified products
        /// </summary>
        /// <param name="productIdentifiers"></param>
        public void ResetProducts(string[] productIdentifiers)
        {
            if (productIdentifiers == null)
                return;

            for (int i = 0; i < productIdentifiers.Length; i++)
            {
                InAppProduct product = Find(productIdentifiers[i]);
                if (product != null)
                    product.Reset();
            }
        }

        /// <summary>
        /// Gets array of in-app products
        /// </summary>
        /// <returns></returns>
        public InAppProduct[] ToArray()
        {
            return _products.ToArray();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection
        /// </summary>
        /// <returns></returns>
        public IEnumerator<InAppProduct> GetEnumerator()
        {
            return _products.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
