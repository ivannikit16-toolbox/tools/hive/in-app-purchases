﻿#if HIVE_INAPP_PURCHASES_ENABLED && UNITY_IOS

using Hive.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hive.InAppPurchases
{
    internal sealed class InAppPurchases_iOS : InAppPurchases
    {
#region Native

        private const string JSON_KEY_RESULT_CODE     = "resultCode";
        private const string JSON_KEY_REQUEST_ID      = "requestId";
        private const string JSON_KEY_DATA            = "data";
        private const string JSON_KEY_PRODUCT_ID      = "productId";
        private const string JSON_KEY_TRANSACTION_ID  = "transactionId";
        private const string JSON_KEY_INTENTION_ID    = "intentId";
        private const string JSON_KEY_PRICE           = "price";
        private const string JSON_KEY_FORMATTED_PRICE = "formattedPrice";
        private const string JSON_KEY_COUNRTY_CODE    = "countryCode";
        private const string JSON_KEY_CURRENCY_CODE   = "curCode";
        private const string JSON_KEY_CURRENCY_SYMBOL = "curSymb";
        private const string JSON_KEY_RECEIPT         = "receipt";
        private const string JSON_KEY_SUBSCRIPTON_ACTIVE    = "sa";
        private const string JSON_KEY_PURCHASING_DATE       = "spd";
        private const string JSON_KEY_EXPIRATION_DATE       = "sed";
        private const string JSON_KEY_CANCELLATION_DATE     = "scd";
        private const string JSON_KEY_ORIGINAL_TRANSACTION_ID = "oTransactionId";

        // result codes
        private const int IapResultCode_None = 0;
        private const int IapResultCode_OK = 1;
        private const int IapResultCode_Cancelled = 2;
        private const int IapResultCode_Failed = 3;
        private const int IapResultCode_ServiceUnavailable = 4;
        private const int IapResultCode_ItemUnavailable = 5;
        private const int IapResultCode_ItemAlreadyOwned = 6;
        private const int IapResultCode_InvalidPayment = 7;
        private const int IapResultCode_InvalidClient = 8;
        private const int IapResultCode_VerificationFailed = 9;
        private const int IapResultCode_UnknownException = 100;
        private const int IapResultCode_InvalidOperationException = 101;
        private const int IapResultCode_BadRequestException = 102;


        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern int hiveInAppPurchases_getLogLevel();

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern void hiveInAppPurchases_setLogLevel(int level);

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern bool hiveInAppPurchases_getEnableLocalReceiptVerifier();

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern void hiveInAppPurchases_setEnableLocalReceiptVerifier(bool value);

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern bool hiveInAppPurchases_getEnableItunesReceiptVerifier();

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern void hiveInAppPurchases_setEnableItunesReceiptVerifier(bool value);


        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern void hiveInAppPurchases_setPurchaseIntentionsChangedCallback(StringResponseHandler callback);

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern void hiveInAppPurchases_setSubscriptionsChangedCallback(StringResponseHandler callback);

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern void hiveInAppPurchases_setSubscriptionsRenewedCallback(StringResponseHandler callback);

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern void hiveInAppPurchases_setSubscriptionProductIdentifiers(string[] ids, int count);

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern void hiveInAppPurchases_start();


        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern bool hiveInAppPurchases_canMakePayment();

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern int hiveInAppPurchases_requestProductsDataByIds(string[] ids, int count, StringResponseHandler callback);

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern int hiveInAppPurchases_purchaseProduct(string productId, int quantity, StringResponseHandler callback);

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern int hiveInAppPurchases_restorePurchases(StringResponseHandler callback);

        [System.Runtime.InteropServices.DllImport("__Internal")]
        private static extern int hiveInAppPurchases_processPurchaseIntention(int intentionId, StringResponseHandler callback);

        private static InAppResultCodes GetResultCode(int nativeCode)
        {
            switch (nativeCode)
            {
                case IapResultCode_OK: return InAppResultCodes.Ok;
                case IapResultCode_Cancelled: return InAppResultCodes.Cancelled;
                case IapResultCode_Failed: return InAppResultCodes.Failed;
                case IapResultCode_ServiceUnavailable: return InAppResultCodes.ServiceUnavailabe;
                case IapResultCode_ItemUnavailable: return InAppResultCodes.ItemUnavailable;
                case IapResultCode_ItemAlreadyOwned: return InAppResultCodes.ItemAlreadyOwned;
                case IapResultCode_InvalidPayment: return InAppResultCodes.Failed;
                case IapResultCode_InvalidClient: return InAppResultCodes.Failed;
                case IapResultCode_VerificationFailed: return InAppResultCodes.VerificationFailed;
                case IapResultCode_InvalidOperationException: return InAppResultCodes.InvalidOperationException;
                case IapResultCode_BadRequestException: return InAppResultCodes.BadRequestException;
                default: return InAppResultCodes.UnknownException;
            }
        }

        private static InAppEventArgs CreateEventArgs(JsonNode json)
        {
            InAppResultCodes rc = GetResultCode(json.OptInt32(JSON_KEY_RESULT_CODE, -1));
            return new InAppEventArgs(rc);
        }

        private static void FillProductData(InAppProduct product, JsonNode data)
        {
            if (product == null || data == null)
                return;

            product.State          = InAppProductState.Updated;
            product.FormattedPrice = data.OptString(JSON_KEY_FORMATTED_PRICE);
            product.CountryCode    = data.OptString(JSON_KEY_COUNRTY_CODE);
            product.CurrencyCode   = data.OptString(JSON_KEY_CURRENCY_CODE);
            product.CurrencySymbol = data.OptString(JSON_KEY_CURRENCY_SYMBOL);
            product.Price          = data.OptSingle(JSON_KEY_PRICE);
        }

#endregion

#region Requests

        private class Request
        {
            public int RequestId;
            public InAppProduct[] Products;
            public string Payload;
            public InAppEventHandler callback;
        }

        private static Dictionary<int, Request> _requests = new Dictionary<int, Request>();

        private static bool ParseResponse(string msg, out JsonNode json, out Request req)
        {
            req = null;
            json = null;

            if (string.IsNullOrEmpty(msg))
                return false;

            json = JsonNode.ParseJsonString(msg);
            int reqId = json.OptInt32(JSON_KEY_REQUEST_ID, -1);

            if (reqId >= 0 && _requests.TryGetValue(reqId, out req))
            {
                _requests.Remove(reqId);
                return true;
            }

            return false;
        }

        private Request AddRequest(int reqId, InAppEventHandler callback)
        {
            Request req = new Request();
            req.RequestId = reqId;
            req.callback = callback;
            _requests.Add(reqId, req);
            return req;
        }

#endregion

#region Implementation

        protected override bool SupportedImpl
        {
            get { return true; }
        }

        protected override LogLevel LogLevelImpl
        {
            get { return (LogLevel)hiveInAppPurchases_getLogLevel(); }
            set { hiveInAppPurchases_setLogLevel((int)value); }
        }

        protected override bool CanMakePaymentImpl
        {
            get { return hiveInAppPurchases_canMakePayment(); }
        }

        protected override void StartImpl(InAppPurchasesExtension settings)
        {
            hiveInAppPurchases_setEnableLocalReceiptVerifier(settings.EnableLocalReceiptVerifier);
            hiveInAppPurchases_setEnableItunesReceiptVerifier(settings.EnableItunesReceiptVerifier);

            string[] subscriptionsIds = Products
                .Where(p => p.ProductType == InAppProductType.AutoRenewableSubscription)
                .Select(p => p.Identifier)
                .ToArray();
            hiveInAppPurchases_setSubscriptionProductIdentifiers(subscriptionsIds, subscriptionsIds.Length);

            hiveInAppPurchases_setPurchaseIntentionsChangedCallback(OnPurchaseIntentionsChangedHandler);
            hiveInAppPurchases_setSubscriptionsChangedCallback(OnSubscriptionsChangedHandler);
            hiveInAppPurchases_setSubscriptionsRenewedCallback(OnSubscriptionRenewedHandler);

            hiveInAppPurchases_start();
        }

        protected override void RequestProductsDataImpl(InAppProduct[] products, string[] productIds, InAppEventHandler callback)
        {
            int reqId = hiveInAppPurchases_requestProductsDataByIds(productIds, productIds.Length, OnProductsDetailsReceived);
            if (reqId < 0)
            {
                if (callback != null)
                    callback(this, new InAppEventArgs(InAppResultCodes.BadRequestException));
                return;
            }

            Request request = AddRequest(reqId, callback);
            request.Products = products;
        }

        protected override void PurchaseProductImpl(InAppProduct product, string payload, InAppEventHandler callback)
        {
            int reqId = hiveInAppPurchases_purchaseProduct(product.Identifier, 1, OnPurchaseProductResult);
            if (reqId < 0)
            {
                if (callback != null)
                    callback(this, new InAppEventArgs(InAppResultCodes.BadRequestException));
                return;
            }

            Request req = AddRequest(reqId, callback);
            req.Products = new[] { product };
            req.Payload = payload;
        }

        protected override void RestorePurchasesImpl(InAppEventHandler callback)
        {
            int reqId = hiveInAppPurchases_restorePurchases(OnRestorePurchasesResult);
            if (reqId < 0)
            {
                if (callback != null)
                    callback(this, new InAppEventArgs(InAppResultCodes.BadRequestException));
                return;
            }

            AddRequest(reqId, callback);
        }

        protected override void ProcessPurchaseIntentionImpl(InAppPurchaseIntention intention, InAppEventHandler callback)
        {
            int reqId = hiveInAppPurchases_processPurchaseIntention(intention.Identifier, OnPurchaseProductResult);
            if (reqId < 0)
            {
                if (callback != null)
                    callback(this, new InAppEventArgs(InAppResultCodes.BadRequestException));
                return;
            }

            AddRequest(reqId, callback).Products = new[] { Products.Find(intention.ProductIdentifier) };
        }

#endregion

#region Responses

        [AOT.MonoPInvokeCallback(typeof(StringResponseHandler))]
        private static void OnProductsDetailsReceived(int reqId, string msg)
        {
            JsonNode json;
            Request req;
            if (!ParseResponse(msg, out json, out req))
                return;

            InAppEventArgs args = CreateEventArgs(json);
            args.Products = req.Products;

            // Fill new data
            if (args.Succeeded)
            {
                JsonArray data = json.OptArray(JSON_KEY_DATA);
                if (data != null)
                {
                    for (int i = 0; i < data.Count; i++)
                    {
                        json = data[i];
                        InAppProduct prod = args.FindProduct(json.OptString(JSON_KEY_PRODUCT_ID));
                        if (prod == null)
                            continue;

                        FillProductData(prod, json);
                    }
                }
            }

            if (req.callback != null)
                req.callback(_instance, args);
        }

        [AOT.MonoPInvokeCallback(typeof(StringResponseHandler))]
        private static void OnPurchaseProductResult(int reqId, string msg)
        {
            JsonNode json;
            Request req;
            if (!ParseResponse(msg, out json, out req))
                return;

            InAppEventArgs args = CreateEventArgs(json);
            args.Products = req.Products;
            args.Payload = req.Payload;

            if (args.Succeeded)
            {
                JsonObject data = json.OptObject(JSON_KEY_DATA);
                if (data != null)
                {
                    args.Receipt = data.OptString(JSON_KEY_RECEIPT);
                    args.TransactionId = data.OptString(JSON_KEY_TRANSACTION_ID);
                    args.OriginalTransactionId = data.OptString(JSON_KEY_ORIGINAL_TRANSACTION_ID);
                }
            }

            if (req.callback != null)
                req.callback(_instance, args);
        }

        [AOT.MonoPInvokeCallback(typeof(StringResponseHandler))]
        private static void OnRestorePurchasesResult(int reqId, string msg)
        {
            JsonNode json;
            Request req;
            if (!ParseResponse(msg, out json, out req))
                return;

            InAppEventArgs args = CreateEventArgs(json);
            if (args.Succeeded)
            {
                JsonArray data = json.OptArray(JSON_KEY_DATA);
                if (data != null)
                {
                    List<InAppProduct> products = new List<InAppProduct>(data.Count);
                    for (int i = 0; i < data.Count; i++)
                    {
                        InAppProduct product = Products.Find(data[i].AsString);
                        if (product != null)
                            products.Add(product);
                    }

                    args.Products = products.ToArray();
                }

                args.Receipt = json.OptString(JSON_KEY_RECEIPT); // stored inside "json" object, not in "data"
            }

            if (req.callback != null)
                req.callback(_instance, args);
        }

        [AOT.MonoPInvokeCallback(typeof(StringResponseHandler))]
        private static void OnPurchaseIntentionsChangedHandler(int reqId, string msg)
        {
            if (string.IsNullOrEmpty(msg))
                return;

            JsonNode json = null;
            if (!JsonNode.TryParseJsonString(msg, out json))
                return;

            JsonArray data = json.OptArray(JSON_KEY_DATA);
            if (data == null)
                return;

            List<InAppPurchaseIntention> intentions = new List<InAppPurchaseIntention>(data.Count);
            for (int i = 0; i < data.Count; i++)
            {
                JsonObject intentionData = data.OptObject(i);
                if (intentionData == null)
                    continue;

                intentions.Add(new InAppPurchaseIntention(
                    intentionData.GetInt32(JSON_KEY_INTENTION_ID),
                    intentionData.GetString(JSON_KEY_PRODUCT_ID),
                    intentionData.OptString(JSON_KEY_FORMATTED_PRICE),
                    intentionData.OptString(JSON_KEY_CURRENCY_CODE),
                    intentionData.OptString(JSON_KEY_CURRENCY_SYMBOL),
                    intentionData.OptSingle(JSON_KEY_PRICE)
                    ));
                //FillProductData(product, json);
            }

            OnPurchaseIntentionsChanged(intentions.ToArray());
        }

        [AOT.MonoPInvokeCallback(typeof(StringResponseHandler))]
        private static void OnSubscriptionsChangedHandler(int reqId, string msg)
        {
            if (string.IsNullOrEmpty(msg))
                return;

            JsonNode json = null;
            if (!JsonNode.TryParseJsonString(msg, out json))
                return;

            JsonObject data = json.OptObject(JSON_KEY_DATA);
            if (data == null)
                return;
            
            JsonArray arr = data.OptArray("subs");
            if (arr == null)
                return;

            List<InAppProduct> subscriptions = new List<InAppProduct>(arr.Count);
            for (int i = 0; i < arr.Count; i++)
            {
                JsonObject subscriptionData = arr.OptObject(i);
                if (subscriptionData == null)
                    continue;

                InAppProduct product = Products.Find(subscriptionData.OptString(JSON_KEY_PRODUCT_ID));
                if (product == null)
                    continue;

                product.SubscriptionActive = subscriptionData.OptBoolean(JSON_KEY_SUBSCRIPTON_ACTIVE, false);
                product.SubscriptionPurchasingDate = GetDateFromJsonObject(subscriptionData, JSON_KEY_PURCHASING_DATE);
                product.SubscriptionExpirationDate = GetDateFromJsonObject(subscriptionData, JSON_KEY_EXPIRATION_DATE);
                product.SubscriptionCancellationDate = GetDateFromJsonObject(subscriptionData, JSON_KEY_CANCELLATION_DATE);
                product.SubscriptionTransactionId = subscriptionData.OptString(JSON_KEY_TRANSACTION_ID);
                product.SubscriptionOriginalTransactionId = subscriptionData.OptString(JSON_KEY_ORIGINAL_TRANSACTION_ID);
                subscriptions.Add(product);
            }

            InAppEventArgs args = CreateEventArgs(json);
            args.Products = subscriptions.ToArray();
            args.Receipt = data.OptString(JSON_KEY_RECEIPT);
            OnSubscriptionsChanged(args);
        }

        [AOT.MonoPInvokeCallback(typeof(StringResponseHandler))]
        private static void OnSubscriptionRenewedHandler(int reqId, string msg)
        {
            if (string.IsNullOrEmpty(msg))
                return;

            JsonNode json = null;
            if (!JsonNode.TryParseJsonString(msg, out json))
                return;

            JsonObject data = json.OptObject(JSON_KEY_DATA);
            if (data == null)
                return;

            InAppProduct product = Products.Find(data.OptString(JSON_KEY_PRODUCT_ID));
            if (product == null)
                return;

            InAppEventArgs args = CreateEventArgs(json);
            args.Products = new[] { product };
            args.TransactionId = data.OptString(JSON_KEY_TRANSACTION_ID);
            args.OriginalTransactionId = data.OptString(JSON_KEY_ORIGINAL_TRANSACTION_ID);
            args.Receipt = data.OptString(JSON_KEY_RECEIPT);

            OnSubscriptionRenewed(args);
        }

    private static DateTime? GetDateFromJsonObject(JsonObject json, string key)
        {
            ulong value = json.OptUInt64(key, 0);
            if (value == 0)
                return null;

            return DateTimeExtension.FromUnixTimestamp(value);
        }

#endregion
    }
}

#endif
