﻿namespace Hive.InAppPurchases
{
    public class InAppPurchaseIntention
    {
        public readonly int Identifier;
        public readonly string ProductIdentifier;
        public readonly string FormattedPrice = null;
        public readonly string CurrencyCode = null;
        public readonly string CurrencySymbol = null;
        public readonly float Price = 0f;


        internal InAppPurchaseIntention(
            int identifier,
            string productIdentifier,
            string formattedPrice = null,
            string currencyCode = null,
            string currencySymbol = null,
            float price = 0f)
        {
            Identifier = identifier;
            ProductIdentifier = productIdentifier;
            FormattedPrice = formattedPrice;
            CurrencyCode = currencyCode;
            CurrencySymbol = currencySymbol;
            Price = price;
        }

        public override string ToString()
        {
            return string.Format("InAppPurchaseIntention. Id: {0}, ProductIdentifier: {1}, FormattedPrice: {2}", Identifier, ProductIdentifier, FormattedPrice);
        }
    }
}
