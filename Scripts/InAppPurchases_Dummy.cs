﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace Hive.InAppPurchases
{
    internal sealed class InAppPurchases_Dummy : InAppPurchases
    {
        private bool _simulate = false;
        private InAppResultCodes _simulateResult = InAppResultCodes.NotSupported;

        protected override bool SupportedImpl
        {
            get { return false; }
        }

        protected override LogLevel LogLevelImpl
        {
            get { return LogLevel.Disabled; }
            set { }
        }

        protected override void StartImpl(InAppPurchasesExtension settings)
        {
            // simulate SubscriptionsChanged event on application start
            // as it happens on real device
            StartCoroutine(SimulateSubscriptionsChangedRoutine(1f));

#if UNITY_EDITOR && HIVE_INAPP_PURCHASES_ENABLED
            if (settings.SimulateInEditor)
            {
                _simulate = true;
                _simulateResult = settings.SimulatedOperationsResult;
            }
#endif
        }

        protected override bool CanMakePaymentImpl
        {
            get { return true; }
        }


        protected override void PurchaseProductImpl(InAppProduct product, string payload, InAppEventHandler callback)
        {
            ReturnResult(callback, payload, true, product);
        }

        protected override void RequestProductsDataImpl(InAppProduct[] products, string[] productIds, InAppEventHandler callback)
        {
            ReturnResult(callback, null, false, products);
        }

        protected override void RestorePurchasesImpl(InAppEventHandler callback)
        {
            ReturnResult(callback, null, true);
        }

        protected override void ProcessPurchaseIntentionImpl(InAppPurchaseIntention intention, InAppEventHandler callback)
        {
            ReturnResult(callback, null, true);
        }

        private void ReturnResult(InAppEventHandler callback, string payload, bool updateSubscriptions, params InAppProduct[] products)
        {
            if (callback == null)
                return;

            InAppEventArgs args = new InAppEventArgs(_simulateResult);
            args.Products = products;
            args.Payload = payload;

            if (_simulate)
                StartCoroutine(ReturnResultRoutine(UnityEngine.Random.Range(1f, 3f), updateSubscriptions, callback, args));
            else
                callback(this, args);

        }

        private IEnumerator ReturnResultRoutine(float delay, bool updateSubscriptions, InAppEventHandler callback, InAppEventArgs args)
        {
            yield return new WaitForSeconds(delay);

            if (updateSubscriptions && args.Succeeded && args.Products != null)
            {
                InAppProduct[] subs = args.Products
                    .Where(p => p.ProductType == InAppProductType.AutoRenewableSubscription)
                    .ToArray();

                if (subs.Length > 0)
                    OnSubscriptionsChanged(new InAppEventArgs(_simulateResult) { Products = subs });
            }
            callback(this, args);
        }

        private IEnumerator SimulateSubscriptionsChangedRoutine(float delay)
        {
            yield return new WaitForSeconds(delay);

            var subscriptions = Products
                .Where(product => product.ProductType == InAppProductType.AutoRenewableSubscription)
                .ToArray();

            OnSubscriptionsChanged(new InAppEventArgs(_simulateResult) { Products = subscriptions });
        }
    }
}
