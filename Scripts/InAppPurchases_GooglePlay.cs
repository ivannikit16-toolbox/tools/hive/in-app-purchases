﻿#if HIVE_INAPP_PURCHASES_ENABLED && UNITY_ANDROID && HIVE_GOOGLEPLAY
using Hive.Android;
using Hive.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Hive.InAppPurchases
{
    internal sealed class InAppPurchases_GooglePlay : InAppPurchases
    {
        private bool _canMakePayment = false;

        #region Native

        private AndroidJavaClass _nativeClass = null;

        // request
        public const string JSON_KEY_REQUEST_ID = "requestId";
        public const string JSON_KEY_RESULT_CODE = "resultCode";
        public const string JSON_KEY_RESULT_MESSAGE = "resultMessage";
        public const string JSON_KEY_DATA = "data";

        // product data
        public const string JSON_KEY_PRODUCT_ID = "productId";
        public const string JSON_KEY_PRODUCT_PRICE = "price";
        public const string JSON_KEY_PRODUCT_TYPE = "type";
        public const string JSON_KEY_TITLE = "title";
        public const string JSON_KEY_DESCRIPTION = "description";
        public const string JSON_KEY_CURRENCY_CODE = "price_currency_code";
        public const string JSON_KEY_CURRENCY_SYMBOL = "price_currency_symbol";
        public const string JSON_KEY_PRICE_VALUE = "price_amount_micros";

        // purchase data
        public const string JSON_KEY_PURCHASE_DATA = "purchaseData";
        public const string JSON_KEY_SIGNATURE = "signature";
        public const string JSON_KEY_ORDER_ID = "orderId";
        public const string JSON_KEY_PURCHASE_TIME = "purchaseTime";
        public const string JSON_KEY_PURCHASE_STATE = "purchaseState";

        // result codes
        private const int IAP_OK = 0;
        private const int IAP_USER_CANCELED = 1;
        private const int IAP_BILLING_UNAVAILABLE = 3;
        private const int IAP_ITEM_UNAVAILABLE = 4;
        private const int IAP_DEVELOPER_ERROR = 5;
        private const int IAP_ERROR = 6;
        private const int IAP_ITEM_ALREADY_OWNED = 7;
        private const int IAP_ITEM_NOT_OWNED = 8;
        private const int IAP_UNKNOWN_EXCEPTION = -1000;
        private const int IAP_REMOTE_EXCEPTION = -1001;
        private const int IAP_INVALID_OPERATION_EXCEPTION = -1002;
        private const int IAP_OBJECT_DISPOSED_EXCEPTION = -1003;
        private const int IAP_BAD_RESPONSE_EXCEPTION = -1004;
        private const int IAP_NOT_SUPPORTED_EXCEPTION = -1005;
        private const int IAP_BUSY = -1100;
        private const int IAP_VERIFICATION_FAILED = -1101;

        private InAppResultCodes GetResultCode(int nativeCode)
        {
            switch (nativeCode)
            {
                case IAP_OK:
                    return InAppResultCodes.Ok;
                case IAP_BUSY:
                    return InAppResultCodes.Failed;
                case IAP_ITEM_NOT_OWNED:
                    return InAppResultCodes.Failed;
                case IAP_USER_CANCELED:
                    return InAppResultCodes.Cancelled;
                case IAP_VERIFICATION_FAILED:
                    return InAppResultCodes.VerificationFailed;
                case IAP_BILLING_UNAVAILABLE:
                    return InAppResultCodes.ServiceUnavailabe;
                case IAP_ITEM_UNAVAILABLE:
                    return InAppResultCodes.ItemUnavailable;
                case IAP_ITEM_ALREADY_OWNED:
                    return InAppResultCodes.ItemAlreadyOwned;
                case IAP_NOT_SUPPORTED_EXCEPTION:
                    return InAppResultCodes.NotSupported;
                case IAP_INVALID_OPERATION_EXCEPTION:
                    return InAppResultCodes.InvalidOperationException;
                default:
                    return InAppResultCodes.UnknownException;
            }
        }

        private InAppEventArgs CreateEventArgs(JsonNode json)
        {
            InAppResultCodes rc = GetResultCode(json.OptInt32(JSON_KEY_RESULT_CODE, -1));
            return new InAppEventArgs(rc, json.OptString(JSON_KEY_RESULT_MESSAGE));
        }

        // Listener class
        private class InAppPurchasesListener : AndroidJavaProxy
        {
            public InAppPurchasesListener() : base("org.hive.iap.googleplay.IapHelperListener") { }

            public void onStarted(int resultCode)
            {
                InAppPurchases_GooglePlay iap = ((InAppPurchases_GooglePlay)_instance);
                iap._canMakePayment = resultCode == IAP_OK;
                //iap.UpdateSubscriptions();
            }

            void onProductsDetailsReceived(int reqId, string json)
            {
                ((InAppPurchases_GooglePlay)_instance).OnProductsDetailsReceived(reqId, json);
            }

            void onPurchaseItemResult(int reqId, string json)
            {
                ((InAppPurchases_GooglePlay)_instance).OnPurchaseItemResult(reqId, json);
            }

            void onPurchasedItemsReceived(int reqId, string json)
            {
                ((InAppPurchases_GooglePlay)_instance).OnRestorePurchasesResult(reqId, json);
            }

            //void onSubscriptionsChanged(int reqId, string json)
            //{
            //    ((InAppPurchases_GooglePlay)_instance).OnSubscriptionsChanged(reqId, json);
            //}
        }

        #endregion

        #region Requests

        private class Request
        {
            public int RequestId;
            public InAppProduct[] Products;
            public string Payload;
            public InAppEventHandler callback;
        }

        private Dictionary<int, Request> _requests = new Dictionary<int, Request>();

        private bool ParseResponse(string msg, out JsonNode json, out Request req)
        {
            req = null;
            json = null;

            if (string.IsNullOrEmpty(msg))
                return false;

            json = JsonNode.ParseJsonString(msg);
            int reqId = json.OptInt32(JSON_KEY_REQUEST_ID, -1);

            if (reqId >= 0 && _requests.TryGetValue(reqId, out req))
            {
                _requests.Remove(reqId);
                return true;
            }

            return false;
        }

        private Request AddRequest(int reqId, InAppEventHandler callback)
        {
            Request req = new Request();
            req.RequestId = reqId;
            req.callback = callback;
            _requests.Add(reqId, req);
            return req;
        }

        #endregion

        #region Implementation

        protected override bool SupportedImpl
        {
            get { return true; }
        }

        protected override LogLevel LogLevelImpl
        {
            get
            {
                using (AndroidJavaClass jc = new AndroidJavaClass("org.hive.iap.googleplay.Log"))
                    return (LogLevel)jc.CallStatic<int>("getLevel");
            }

            set
            {
                using (AndroidJavaClass jc = new AndroidJavaClass("org.hive.iap.googleplay.Log"))
                    jc.CallStatic("setLevel", (int)value);
            }
        }

        protected override void StartImpl(InAppPurchasesExtension settings)
        {
            string[] subscriptionsIds = Products
                .Where(p => p.ProductType == InAppProductType.AutoRenewableSubscription)
                .Select(p => p.Identifier)
                .ToArray();

            _nativeClass = new AndroidJavaClass("org.hive.iap.googleplay.IapHelper");
            _nativeClass.CallStatic("setListener", new InAppPurchasesListener());
            _nativeClass.CallStatic("setSubscriptionsChangedCallback", new AndroidCallbackProxy<int, string>(OnSubscriptionsChanged));
            //_nativeClass.CallStatic("setPurchaseIntentionsChangedCallback", OnPurchaseIntentionsChangedHandler);

            _nativeClass.CallStatic("start", (object)subscriptionsIds);
        }

        protected override bool CanMakePaymentImpl
        {
            get { return _canMakePayment; }
        }

        protected override void RequestProductsDataImpl(InAppProduct[] products, string[] productIds, InAppEventHandler callback)
        {
            int reqId = _nativeClass.CallStatic<int>("requestProductsDetails", (object)productIds);
            Request req = AddRequest(reqId, callback);
            req.Products = products;
        }

        protected override void PurchaseProductImpl(InAppProduct product, string payload, InAppEventHandler callback)
        {
            int reqId = -1;

            switch (product.ProductType)
            {
                case InAppProductType.Consumable:
                    reqId = _nativeClass.CallStatic<int>("purchaseConsumable", product.Identifier, payload);
                    break;

                case InAppProductType.NonConsumable:
                    reqId = _nativeClass.CallStatic<int>("purchaseNonConsumable", product.Identifier, payload);
                    break;

                case InAppProductType.AutoRenewableSubscription:
                    reqId = _nativeClass.CallStatic<int>("purchaseSubscription", product.Identifier, payload);
                    break;

                default:
                    if (callback != null)
                        callback(this, new InAppEventArgs(InAppResultCodes.BadRequestException,
                            string.Format("Product type {0} is not supported", product.ProductType))
                        { Payload = payload });
                    return;
            }

            Request req = AddRequest(reqId, callback);
            req.Products = new[] { product };
            req.Payload = payload;
        }

        protected override void RestorePurchasesImpl(InAppEventHandler callback)
        {
            int reqId = _nativeClass.CallStatic<int>("requestPurchasedItems");
            AddRequest(reqId, callback);
        }

        protected override void ProcessPurchaseIntentionImpl(InAppPurchaseIntention intention, InAppEventHandler callback)
        {
            if (callback != null)
                callback(this, new InAppEventArgs(InAppResultCodes.NotSupported));
        }

        private void UpdateSubscriptions()
        {
            //InAppProduct[] subscriptions = Products
            //    .Where(p => p.ProductType == InAppProductType.AutoRenewableSubscription)
            //    .ToArray();

            //string[] subscriptionIds = subscriptions
            //    .Select(p => p.Identifier)
            //    .ToArray();

            _nativeClass.CallStatic<int>("updateSubscriptionsStatus");
        }

        #endregion

        #region Responses

        private void OnProductsDetailsReceived(int reqId, string msg)
        {
            JsonNode json;
            Request req;
            if (!ParseResponse(msg, out json, out req))
                return;

            InAppEventArgs args = CreateEventArgs(json);
            args.Products = req.Products;

            // Fill new data
            if (args.Succeeded)
            {
                JsonArray data = json.OptArray(JSON_KEY_DATA);
                if (data != null)
                {
                    for (int i = 0; i < data.Count; i++)
                    {
                        json = data[i];
                        InAppProduct prod = args.FindProduct(json.OptString(JSON_KEY_PRODUCT_ID));
                        if (prod == null)
                            continue;

                        prod.State = InAppProductState.Updated;
                        prod.FormattedPrice = json.OptString(JSON_KEY_PRODUCT_PRICE);
                        prod.CurrencyCode = json.OptString(JSON_KEY_CURRENCY_CODE);
                        prod.CurrencySymbol = json.OptString(JSON_KEY_CURRENCY_SYMBOL);
                        prod.Price = json.OptSingle(JSON_KEY_PRICE_VALUE);
                    }
                }
            }

            if (req.callback != null)
                req.callback(this, args);
        }

        private void OnPurchaseItemResult(int reqId, string msg)
        {
            JsonNode json;
            Request req;
            if (!ParseResponse(msg, out json, out req))
                return;

            InAppEventArgs args = CreateEventArgs(json);
            args.Products = req.Products;
            args.Payload = req.Payload;

            if (args.Succeeded)
            {
                JsonObject data = json.OptObject(JSON_KEY_DATA);
                if (data != null)
                {
                    args.Receipt = data.OptString(JSON_KEY_PURCHASE_DATA);
                    args.Signature = data.OptString(JSON_KEY_SIGNATURE);
                    args.TransactionId = data.OptString(JSON_KEY_ORDER_ID);
                }
            }

            if (req.callback != null)
                req.callback(this, args);
        }

        private void OnRestorePurchasesResult(int reqId, string msg)
        {
            JsonNode json;
            Request req;
            if (!ParseResponse(msg, out json, out req))
                return;

            InAppEventArgs args = CreateEventArgs(json);
            if (args.Succeeded)
            {
                JsonArray data = json.OptArray(JSON_KEY_DATA);
                if (data != null)
                {
                    List<InAppProduct> products = new List<InAppProduct>(data.Count);
                    for (int i = 0; i < data.Count; i++)
                    {
                        InAppProduct product = Products.Find(data[i].AsString);
                        if (product != null)
                            products.Add(product);
                    }

                    args.Products = products.ToArray();
                }
            }

            if (req.callback != null)
                req.callback(this, args);
        }

        private void OnSubscriptionsChanged(int reqId, string msg)
        {
            if (string.IsNullOrEmpty(msg))
                return;

            JsonNode json = null;
            if (!JsonNode.TryParseJsonString(msg, out json))
                return;

            JsonArray data = json.OptArray(JSON_KEY_DATA);
            if (data == null)
                return;

            List<InAppProduct> subscriptions = new List<InAppProduct>(data.Count);
            for (int i = 0; i < data.Count; i++)
            {
                JsonObject subscriptionData = data.OptObject(i);
                if (subscriptionData == null)
                    continue;

                InAppProduct product = Products.Find(subscriptionData.OptString(JSON_KEY_PRODUCT_ID));
                if (product == null)
                    continue;

                product.SubscriptionActive = subscriptionData.GetInt32(JSON_KEY_PURCHASE_STATE) == 0;
                product.SubscriptionPurchasingDate = GetDateFromJsonObject(subscriptionData, JSON_KEY_PURCHASE_TIME);
                subscriptions.Add(product);
            }

            InAppEventArgs args = CreateEventArgs(json);
            args.Products = subscriptions.ToArray();
            OnSubscriptionsChanged(args);
        }

        private static DateTime? GetDateFromJsonObject(JsonObject json, string key)
        {
            if (string.IsNullOrEmpty(key) || key.Length < 3)
                return null;

            // ms -> s
            key = key.Substring(0, key.Length - 3);

            ulong value = json.OptUInt64(key, 0);
            if (value == 0)
                return null;

            return DateTimeExtension.FromUnixTimestamp(value);
        }

        #endregion
    }
}

#endif
