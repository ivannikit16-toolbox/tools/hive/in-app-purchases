﻿using System;

namespace Hive.InAppPurchases
{
    public enum InAppResultCodes
    {
        // General
        None = 0,
        Ok,
        Cancelled,
        Failed,
        NotSupported,
        ServiceUnavailabe,
        ItemUnavailable,
        ItemAlreadyOwned,
        VerificationFailed,
        CannotMakePayment,

        // Errors
        UnknownException,
        InvalidOperationException,
        BadRequestException,
    }

    public delegate void InAppEventHandler(object sender, InAppEventArgs args);

    public class InAppEventArgs : EventArgs
    {
        public readonly InAppResultCodes ResultCode;
        public readonly string ResultMessage;
        public InAppProduct[] Products;

        /// <summary>
        /// The App Store receipt or Google Play receipt that contains details about the purchase order
        /// </summary>
        public string Receipt = null;

        /// <summary>
        /// Googpe Play: String containing the signature of the purchase data that the developer signed with their private key.
        /// </summary>
        public string Signature = null;

        /// <summary>
        /// iOS only! The string that contains an identifier of purchasing transaction
        /// </summary>
        public string TransactionId = null;

        /// <summary>
        /// iOS only! The string that contains an identifier of original purchasing transaction
        /// </summary>
        public string OriginalTransactionId = null;

        /// <summary>
        /// Additional payload associated with this payment transaction
        /// </summary>
        public string Payload = null;

        public InAppEventArgs(InAppResultCodes result, string message = null)
        {
            ResultCode = result;
            ResultMessage = message;
        }

        public bool Succeeded { get { return ResultCode == InAppResultCodes.Ok || ResultCode == InAppResultCodes.ItemAlreadyOwned; } }

        public InAppProduct FindProduct(string identifier)
        {
            if (Products == null)
                return null;
            for (int i = 0; i < Products.Length; i++)
                if (Products[i].Identifier == identifier)
                    return Products[i];
            return null;
        }

        public override string ToString()
        {
            return string.Format("InAppEventArgs: ResultCode = {0}, Message = {1}", ResultCode.ToString(), ResultMessage);
        }
    }

}
