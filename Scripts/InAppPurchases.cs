﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Hive.InAppPurchases
{
    public abstract class InAppPurchases : MonoBehaviourWrapper
    {
        private static string UNIQUE_NAME = "HiveInAppPurchases";
        protected static InAppPurchases _instance = null;

        private static InAppPurchases Instance
        {
            get
            {
                if (_instance == null)
                {
                    HiveFoundation.AssertStarted();
                    _instance =
#if HIVE_INAPP_PURCHASES_ENABLED && UNITY_EDITOR
                        HiveFoundation.GetExtensionComponentInstance<InAppPurchases_Dummy>(UNIQUE_NAME);
#elif HIVE_INAPP_PURCHASES_ENABLED && UNITY_ANDROID && HIVE_GOOGLEPLAY
                        HiveFoundation.GetExtensionComponentInstance<InAppPurchases_GooglePlay>(UNIQUE_NAME);
#elif HIVE_INAPP_PURCHASES_ENABLED && UNITY_IOS
                        HiveFoundation.GetExtensionComponentInstance<InAppPurchases_iOS>(UNIQUE_NAME);
#else
                        HiveFoundation.GetExtensionComponentInstance<InAppPurchases_Dummy>(UNIQUE_NAME);
#endif
                }

                return _instance;
            }
        }

        protected abstract LogLevel LogLevelImpl { get; set; }
        protected abstract bool SupportedImpl { get; }
        protected abstract void StartImpl(InAppPurchasesExtension settings);
        protected abstract bool CanMakePaymentImpl { get; }
        protected abstract void PurchaseProductImpl(InAppProduct product, string payload, InAppEventHandler callback);
        protected abstract void RestorePurchasesImpl(InAppEventHandler callback);
        protected abstract void RequestProductsDataImpl(InAppProduct[] products, string[] productIds, InAppEventHandler callback);
        protected abstract void ProcessPurchaseIntentionImpl(InAppPurchaseIntention intention, InAppEventHandler callback);

        #region Starting

        private static bool _started = false;

        /// <summary>
        /// Checks support for this on native platform
        /// </summary>
        public static bool Supported
        {
            get { return Instance.SupportedImpl; }
        }

        /// <summary>
        /// Gets state of the InAppPurchases plugin
        /// </summary>
        public static bool Started
        {
            get { return _started; }
        }

        /// <summary>
        /// Runs InAppPurchases plugin
        /// </summary>
        [Obsolete("Start() is deprecated, please use AddInAppPurchase() in Hive Host Builder instead")]
        public static void Start()
        {
            if (_started)
                return;

            try
            {
                var settings = AppProfile.Instance.InAppPurchases;

                Products.ReadOnly = true;

                Instance.StartImpl(settings);
                _started = true;

                _subscriptions = Products
                    .Where(p => p.ProductType == InAppProductType.AutoRenewableSubscription)
                    .ToArray();

                if (RequestProductsDataOnStartup || settings.RequestProductsDataOnStartup)
                    HiveFoundation.Instance.StartCoroutine(RequestProductsDataOnStartupCoroutine());
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        /// <summary>
        /// Runs InAppPurchases plugin
        /// </summary>
        internal static void Run(InAppPurchasesExtension settings)
        {
            if (_started)
                return;

            try
            {
                Products = settings.Products;
                Products.ReadOnly = true;

                Instance.StartImpl(settings);
                _started = true;

                _subscriptions = Products
                    .Where(p => p.ProductType == InAppProductType.AutoRenewableSubscription)
                    .ToArray();

                if (RequestProductsDataOnStartup || settings.RequestProductsDataOnStartup)
                    HiveFoundation.Instance.StartCoroutine(RequestProductsDataOnStartupCoroutine());
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        /// <summary>
        /// Asserts an InAppPurchases plugin is in a valid state 
        /// </summary>
        /// <param name="value">Set true if plugin should be started</param>
        /// <param name="throwError"></param>
        /// <returns></returns>
        internal static bool AssertStarted(bool value, bool throwError)
        {
            if (value == _started)
                return true;

            if (throwError)
                throw new InvalidOperationException(_started
                    ? "It is not permitted to call this method after SDK has been started"
                    : "It is not permitted to call this method before SDK has been started");

            return false;
        }

        private static IEnumerator RequestProductsDataOnStartupCoroutine()
        {
            yield return new WaitForSeconds(2f);

            RequestProductsData(Products.ToArray());
        }

        #endregion

        /// <summary>
        /// Gets collection of products
        /// </summary>
        public static InAppProductsCollection Products
        {
            get { return _products; }
            private set { _products = value; }
        }
        private static InAppProductsCollection _products = new InAppProductsCollection(); // [OBSOLETE] it just for support Start() method

        /// <summary>
        /// Gets or sets whether all products should be updated after plugin start
        /// </summary>
        public static bool RequestProductsDataOnStartup = false; // it's public for support old code

        /// <summary>
        /// Gets or sets a level that defines what kind of messages will be added to the log
        /// </summary>
        public static LogLevel LogLevel
        {
            get { return Instance.LogLevelImpl; }
            set { Instance.LogLevelImpl = value; }
        }

        /// <summary>
        /// Determines whether a user can make payment
        /// </summary>
        public static bool CanMakePayment
        {
            get { return AssertStarted(true, false) && Instance.CanMakePaymentImpl; }
        }

        /// <summary>
        /// Restore purchases
        /// </summary>
        /// <param name="callback"></param>
        public static void RestorePurchases(InAppEventHandler callback)
        {
            AssertStarted(true, true);
            Instance.RestorePurchasesImpl(callback);
        }

        #region Purchase

        /// <summary>
        /// Purchase product
        /// </summary>
        /// <param name="productIdentifier"></param>
        /// <param name="payload"></param>
        /// <param name="callback"></param>
        public static void PurchaseProduct(string productIdentifier, string payload, InAppEventHandler callback)
        {
            AssertStarted(true, true);

            if (callback == null)
            {
                Debug.LogWarningFormat("Purchase of the product with identifier '{0}' is cancelled because callback is null", productIdentifier);
                return;
            }

            if (productIdentifier == null)
            {
                Debug.LogError("Failed to purchase product because its identifier is null");
                callback(Instance, new InAppEventArgs(InAppResultCodes.BadRequestException, "Product idenfigier is null"));
                return;
            }

            InAppProduct product = Products.Find(productIdentifier);
            if (product == null)
            {
                Debug.LogErrorFormat("Failed to purchase product with identifier '{0}' because it doesn't exist in in-app products collection", productIdentifier);
                callback(Instance, new InAppEventArgs(InAppResultCodes.BadRequestException, "Product doesn't exist in in-app products collection"));
                return;
            }

            if (product.ProductType == InAppProductType.Undefined)
            {
                Debug.LogErrorFormat("Failed to purchase product with identifier '{0}' because its type is not defined", productIdentifier);
                callback(Instance, new InAppEventArgs(InAppResultCodes.BadRequestException, "Product type is not defined"));
                return;
            }

            if (!Instance.CanMakePaymentImpl)
            {
                Debug.LogWarningFormat("Failed to purchase product with identifier '{0}' because the user is not allowed to make payment", productIdentifier);
                callback(Instance, new InAppEventArgs(InAppResultCodes.CannotMakePayment));
            }
            else
            {
                Instance.PurchaseProductImpl(product, payload, callback);
            }
        }

        /// <summary>
        /// Purchase product
        /// </summary>
        /// <param name="productIdentifier"></param>
        /// <param name="callback"></param>
        public static void PurchaseProduct(string productIdentifier, InAppEventHandler callback)
        {
            PurchaseProduct(productIdentifier, null, callback);
        }

        /// <summary>
        /// Purchase product
        /// </summary>
        /// <param name="product"></param>
        /// <param name="payload"></param>
        /// <param name="callback"></param>
        public static void PurchaseProduct(InAppProduct product, string payload, InAppEventHandler callback)
        {
            if (product == null)
            {
                Debug.LogError("In-app product is null. Failed to purchase.");
                callback(Instance, new InAppEventArgs(InAppResultCodes.BadRequestException, "Product is null"));
                return;
            }

            PurchaseProduct(product.Identifier, payload, callback);
        }

        /// <summary>
        /// Purchase product
        /// </summary>
        /// <param name="product"></param>
        /// <param name="callback"></param>
        public static void PurchaseProduct(InAppProduct product, InAppEventHandler callback)
        {
            PurchaseProduct(product, null, callback);
        }

        #endregion

        #region Request Products Data

        private static List<InAppProduct> _rpdProducts = new List<InAppProduct>();
        private static List<string> _rpdProductIds = new List<string>();
        private static List<InAppEventHandler> _rpdCallbacks = new List<InAppEventHandler>();
        private static Coroutine _rpdCoroutine = null;
        private static float _rpdPredelay = .1f;

        /// <summary>
        /// Gets or sets the time interval during which all ProductData requests will be combined into one request.
        /// Value 0 means the feature is disabled
        /// </summary>
        public static float RequestProductsDataPredelay
        {
            get { return _rpdPredelay; }
            set { _rpdPredelay = Mathf.Max(0f, value); }
        }

        private static bool AddProductToCoroutine(InAppProduct product)
        {
            if (_rpdProducts.Contains(product))
                return false;

            _rpdProducts.Add(product);
            _rpdProductIds.Add(product.Identifier);
            return true;
        }

        private static void AddCallbackToCoroutine(InAppEventHandler callback)
        {
            if (!_rpdCallbacks.Contains(callback))
                _rpdCallbacks.Add(callback);
        }

        private static IEnumerator RequestProductsDataCoroutine()
        {
            yield return new WaitForSeconds(_rpdPredelay); // skip frame/time

            _rpdCoroutine = null;

            InAppProduct[] products = _rpdProducts.ToArray();
            _rpdProducts.Clear();

            string[] productIdentifiers = _rpdProductIds.ToArray();
            _rpdProductIds.Clear();

            InAppEventHandler callback = _rpdCallbacks.Count > 0 ? _rpdCallbacks[0] : null;
            for (int i = 1; i < _rpdCallbacks.Count; i++)
                callback += _rpdCallbacks[i];
            _rpdCallbacks.Clear();

            Instance.RequestProductsDataImpl(products, productIdentifiers, callback);
        }

        /// <summary>
        /// Requesting data of products.
        /// </summary>
        /// <param name="productIdentifiers">Array of identifiers</param>
        /// <param name="reset">If it true, all products will be reseted to default state</param>
        /// <param name="callback"></param>
        public static void RequestProductsData(string[] productIdentifiers, bool reset, InAppEventHandler callback = null)
        {
            AssertStarted(true, true);

            // check identifiers
            if (productIdentifiers == null || productIdentifiers.Length == 0)
            {
                if (callback != null)
                {
                    InAppEventArgs e = new InAppEventArgs(InAppResultCodes.Ok);
                    callback(Instance, e);
                }
                return;
            }

            // check products
            InAppProduct[] products = Products.Find(productIdentifiers);
            if (products == null || products.Length == 0)
            {
                if (callback != null)
                {
                    Debug.LogError("Failed to request products data. There are no products with specified identifiers in the collection.");
                    InAppEventArgs e = new InAppEventArgs(InAppResultCodes.BadRequestException);
                    callback(Instance, e);
                }
                return;
            }

            // check both arrays
            if (products.Length != productIdentifiers.Length)
                Debug.LogWarning("Collection of products doesn't not contain one or more items requested by method RequestProductsData(productIdentifiers)");

            // prepare products
            bool combine = _rpdPredelay > 0f;
            for (int i = 0; i < products.Length; i++)
            {
                InAppProduct product = products[i];
                if (combine && !AddProductToCoroutine(product)) // work with only unique items
                    continue;

                product.State = InAppProductState.OutOfDate;
                if (reset)
                    product.Reset();
            }

            // Make request or run coroutine
            if (combine)
            {
                AddCallbackToCoroutine(callback);
                if (_rpdCoroutine == null)
                    _rpdCoroutine = Instance.StartCoroutine(RequestProductsDataCoroutine());
            }
            else
                Instance.RequestProductsDataImpl(products, productIdentifiers, callback);
        }

        /// <summary>
        /// Requesting data of products.
        /// All products in will NOT be reseted to default state
        /// </summary>
        /// <param name="productIdentifiers">Array of identifiers</param>
        /// <param name="callback"></param>
        public static void RequestProductsData(string[] productIdentifiers, InAppEventHandler callback = null)
        {
            AssertStarted(true, true);
            RequestProductsData(productIdentifiers, false, callback);
        }

        /// <summary>
        /// Requesting data of products
        /// </summary>
        /// <param name="products">Array of in-app products to request</param>
        /// <param name="reset">is it necessary to reset state and clear data of all in-app products</param>
        /// <param name="callback"></param>
        public static void RequestProductsData(InAppProduct[] products, bool reset, InAppEventHandler callback = null)
        {
            // get identifiers
            string[] productIdentifiers = null;
            if (products != null)
            {
                productIdentifiers = new string[products.Length];
                for (int i = 0; i < products.Length; i++)
                {
                    productIdentifiers[i] = products[i].Identifier;
                    if (reset)
                        products[i].Reset();
                }
            }

            RequestProductsData(productIdentifiers, reset, callback);
        }

        /// <summary>
        /// Requesting data of products.
        /// All products in specified array will NOT be reseted to default state
        /// </summary>
        /// <param name="products">Array of in-app products to request</param>
        /// <param name="callback"></param>
        public static void RequestProductsData(InAppProduct[] products, InAppEventHandler callback = null)
        {
            RequestProductsData(products, false, callback);
        }

        #endregion

        #region Purchase intentions

        /// <summary>
        /// Gets a collection of purchase intentions
        /// </summary>
        public static InAppPurchaseIntention[] PurchaseIntentions { get { return _purchaseIntentions; } }
        private static InAppPurchaseIntention[] _purchaseIntentions = new InAppPurchaseIntention[0];

        public static event Action<InAppPurchaseIntention[]> PurchaseIntentionsChanged;

        protected static void OnPurchaseIntentionsChanged(InAppPurchaseIntention[] purchaseIntentions)
        {
            _purchaseIntentions = purchaseIntentions;
            if (PurchaseIntentionsChanged != null)
                PurchaseIntentionsChanged(purchaseIntentions);
        }

        public static void ProcessPurchaseIntention(InAppPurchaseIntention intention, InAppEventHandler callback)
        {
            if (AssertStarted(true, false))
                Instance.ProcessPurchaseIntentionImpl(intention, callback);
            else if (callback != null)
                callback(Instance, new InAppEventArgs(InAppResultCodes.ServiceUnavailabe, "InAppPurchase plugin doesn't start"));
        }

        #endregion

        #region Subscriptions

        /// <summary>
        /// Gets a collection of subscriptions
        /// </summary>
        public static InAppProduct[] Subscriptions { get { return _subscriptions; } }
        private static InAppProduct[] _subscriptions = new InAppProduct[0];
        //private static InAppProduct[] _lastActiveSubscriptions = null; // items from last native response

        public static event InAppEventHandler SubscriptionsChanged;
        public static event InAppEventHandler SubscriptionRenewed;

        protected static void OnSubscriptionsChanged(InAppEventArgs args)
        {
            //_lastActiveSubscriptions = subscriptions;

            //// reset subscriptions state
            //foreach (InAppProduct subscription in _subscriptions)
            //{
            //    if (subscriptions.FirstOrDefault(p => p.Identifier == subscription.Identifier) == null)
            //        subscription.SubscriptionActive = false;
            //}

            if (SubscriptionsChanged != null)
                SubscriptionsChanged(null, args);
        }

        protected static void OnSubscriptionRenewed(InAppEventArgs args)
        {
            if (SubscriptionRenewed != null)
                SubscriptionRenewed(null, args);
        }

        #endregion
    }
}
